{
  description = "The TUCH webpage";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils, }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
      in
      rec {
        packages.default = pkgs.stdenv.mkDerivation {
          name = "tuch-web";
          src = builtins.path { path = ./.; name = "tuch-web"; };
          nativeBuildInputs = with pkgs; [ zola ];
          buildPhase = "zola build";
          doCheck = true;
          checkPhase = "zola check";
          installPhase = "cp --recursive public $out";
        };

        checks = {
          inherit (packages) default;
          format = pkgs.runCommand "check-format"
            { buildInputs = [ pkgs.nixpkgs-fmt ]; }
            "nixpkgs-fmt --check ${./.} && touch $out";
        };
      });
}
