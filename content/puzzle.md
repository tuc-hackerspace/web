+++
weight = 30
+++

# Its Puzzle-Time!

__Can you solve it?__

```bash
#!/usr/bin/env bash

f=`mktemp`
e=`echo -e '\e'`
s=$((60**2*24*7))
t=`date +%s`
((t=t-s/7))
echo "VGVsZWdyYW0K" > $f

cat <<EOF >&2
$e[2J $e[0;0HHi $(whoami), bist du auf der Suche nach anderen $(uname) Usern?

Falls ja, bist du bei uns genau richtig!
Wir haben eine Gruppe bei $e[34m`base64 -d $f`$e[0m, der du unter folgendem Link beitreten kannst:
`rev <(echo QDmCaAIdY7mvTGDQho3_ZA/tahcnioj/em.t//:sptth)`

Ansonsten findest du uns für gewöhnlich am `date -d@$((t-t%s+s)) -Id`,
ab 19:00 in Raum 202 des D5 (Tannenhöhe, $e[33mIfI$e[0m).

Du bist nicht $e[9mmutig$e[0m dumm genug? Versuchs hier: https://tio.run/#bash
EOF
```

If you happen to be on a device which can no interprete this,
[tio.run](https://tio.run/) might help!
