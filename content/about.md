+++
title = "About us"
weight = 90

[taxonomies]
categories = ["info"]
tags = []
+++

__TUCH__ is a Hackerspace that focusses on computer science related stuff.
It is an acronym for **T**echnische **U**niversität **C**lausthal
**H**ackerspace.

+ Who we are?  
  We are a Hackerspace that is officially recognised by the 
  Clausthal University of Technology.
+ What we do?  
  We work and discuss on computer science related problems.
+ Who can join?  
  Anyone with keen interest and knowledge in computer science and tinkering
  with electronics who wants to expand his or her horizon.
+ When?  
  Thursdays, starting at 7 pm.
+ Location?  
  D5, Room 204, Institute of Informatics, Clausthal University of Technology.

Don't hesitate, get in t\[o\]uch!
